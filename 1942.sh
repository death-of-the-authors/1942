#!/bin/bash

# 1942

TIME=22
DELAY=11

mplayer -ss 5 blanche/blanche.wav &

feh -x -Z --fullscreen --image-bg white --randomize --slideshow-delay=$DELAY tina & disown; sleep $TIME;

feh -x --fullscreen 'neel0.jpg' & disown; sleep $DELAY;

feh -x --fullscreen 'violet0.jpg' & disown; sleep $DELAY;

feh -x -Z --fullscreen --image-bg white --randomize --slideshow-delay=$DELAY tina & disown; sleep $TIME;

feh -x --fullscreen 'violet1.jpg' & disown; sleep $DELAY;

feh -x -Z --fullscreen --image-bg white --randomize --slideshow-delay=$DELAY tina & disown; sleep $TIME;

feh -x --fullscreen 'neel1.jpg' & disown; sleep $DELAY;

feh -x --fullscreen 'violet2.jpg' & disown; sleep $DELAY;

feh -x --fullscreen 'neel3.jpg' & disown; sleep $DELAY;

feh -x -Z --fullscreen --image-bg white --randomize --slideshow-delay=$DELAY tina & disown; sleep $TIME;

feh -x --fullscreen 'violet3.jpg' & disown; sleep $DELAY;

killall -w mplayer;
