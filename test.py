#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from random import *
import nltk
from time import sleep
import sys

# Open all files and do basic cleanup

neel = open('neel.txt', 'r')
neel = neel.read()
neel = neel.replace("\r\n", " ")
neel = neel.replace("\r", " ")
neel = neel.replace("\n", " ")
neel = neel.replace("  ", " ")
neel = neel.replace("\"", " ")
	
violet = open('violet.txt', 'r')
violet = violet.read()
violet = violet.replace("\r\n", " ")
violet = violet.replace("\r", " ")
violet = violet.replace("\n", " ")
violet = violet.replace("  ", " ")
violet = violet.replace("\"", " ")

# Split text into sentences with help of nltk

sent_tokenizer=nltk.data.load('tokenizers/punkt/english.pickle')
sentences_neel = sent_tokenizer.tokenize(neel)

sent_tokenizer=nltk.data.load('tokenizers/punkt/english.pickle')
sentences_violet = sent_tokenizer.tokenize(violet)

# Intro starts here

cmd = "bash intro.sh"
os.system(cmd)

total = 25
rest = 0
extra = 0
currenttotal = 0
amount_selected_elements = 0
element = "images"
time = 215
delay = time/total
dialogues = (sentences_neel, sentences_violet)

# Let the music start and also loop

cmd = "mplayer -ss 5 blanche/blanche.wav &"
os.system(cmd)

while currenttotal < total:
	
	if element == "images":
	
		amount_selected_elements = randint(1, 3)
		
		if currenttotal + amount_selected_elements > total:
			print "stop!"
			break
		else:
			print "currenttotal:", currenttotal, "amount_selected_elements:", amount_selected_elements, "elementtype:", element 
			z = 1
			while z <= amount_selected_elements:
		
				img = str(randint(1, 57)) + ".jpg"
				cmd = "feh -x -Z --fullscreen --image-bg black tina/" + img + " &"
				os.system(cmd)

				sleep(delay)
				z += 1
				print "image: ", img
		
			currenttotal += amount_selected_elements
			element = "texts"

	else:
	
		amount_selected_elements = randint(1, 3)
		
		if currenttotal + amount_selected_elements > total:
			print "stop!"
			break
		
		else:
			print "currenttotal:", currenttotal, "amount_selected_elements:", amount_selected_elements, "elementtype:", element
			y = 1
			while y <= amount_selected_elements:
		
				dialogue = choice(dialogues)
				if dialogue == sentences_neel:
					colour = "firebrick1"
				if dialogue == sentences_violet:
					colour = "DodgerBlue"
				
				dialogue_pos = randrange(len(dialogue))
				slide = str(dialogue[dialogue_pos]) 
				text_file = open("slide.txt", "w")
				text_file.writelines(slide)
				text_file.close()
				img =  "'" + str(y) + "_text.jpg'"
				print "text: ", img

				cmd = "convert -background black -bordercolor black -gravity center -fill " + colour + " -size 1024x768 -border 50 -font OSP-DIN.ttf caption:@slide.txt " + img +"; feh -x --fullscreen " + img + " &"
				os.system(cmd)
			
				sleep(delay)
				y += 1
		
			currenttotal += amount_selected_elements
			element = "images"
			
print "reached end of the loop"
print "currenttotal:", currenttotal, "elementtype:", element 
print "now patch up until you reached total"

while currenttotal < total:
	img = str(randint(1, 57)) + ".jpg"
			
	print "image: ", img
	cmd = "feh -x -Z --fullscreen --image-bg black tina/" + img + " &"
	os.system(cmd)

	sleep(delay)
	currenttotal += 1
	z += 1

print "currenttotal:", currenttotal

# credits start here

cmd = "bash outro.sh"
os.system(cmd)	

# really the end

cmd = "killall -w feh; killall -w mplayer"
os.system(cmd)

print 'FIN'
	
#delay = float(time) / float(i-x)

