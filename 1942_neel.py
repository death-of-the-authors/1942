#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from random import *
import nltk
import time
import sys
from random import shuffle

cmd = "clear"
os.system(cmd)

# keep audience informed/busy. Some kind of intro.
print "1942"
print "Violet, Blanche, Neel and Tina die on different continents, in very different circumstances."
print "70 years later they tell their ever-changing journeys through public and private domains."
print "An unlikely encounter between four women."

# Open all files and do basic cleanup

neel = open('neel.txt', 'r')
neel = neel.read()
neel = neel.replace("\r\n", " ")
neel = neel.replace("\r", " ")
neel = neel.replace("\n", " ")
neel = neel.replace("  ", " ")
neel = neel.replace("\"", " ")

# Split text into sentences with help of nltk

sent_tokenizer=nltk.data.load('tokenizers/punkt/english.pickle')
sentences_neel = sent_tokenizer.tokenize(neel)

# Generate slide: select random sentences_neel and sentences_violet, combine and feed to convert.

i = 0
while i < 3:
	neel_pos = randrange(len(sentences_neel))
	slide = str(sentences_neel[neel_pos])
	text_file = open("slide.txt", "w")
	text_file.writelines(slide)
	text_file.close()
	cmd = "convert -background black -bordercolor black -gravity center -fill white -size 1024x768 -border 10% -font /home/ana/Fonts/UniversElse-Light.ttf caption:@slide.txt slide" + str(i) + ".jpg"
	i += 1
	os.system(cmd)

print "STARTING ..."

cmd = "bash 1942.sh"
os.system(cmd)


