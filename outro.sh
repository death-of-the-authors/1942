#!/bin/bash

DELAY=6
SHORT=1
LONG=9

convert -background black -bordercolor black -gravity center -fill black -size 1280x800 -border 100 -font OSP-DIN.ttf caption:'Patience please!' outro.jpg;
feh -x --fullscreen 'intro.jpg' & disown; sleep $LONG;

convert -background black -bordercolor black -gravity center -pointsize 90 -size 1280 -border 100 -fill white -font OSP-DIN.ttf caption:'sources' outro.jpg;
feh -x --fullscreen 'outro.jpg' & disown; sleep $SHORT;

convert -background black -bordercolor black -gravity center -pointsize 90 -size 1280 -border 100 -fill firebrick1 -font OSP-DIN.ttf caption:'Neel Doff\nJours de Famine et de Détresse\nActes Sud, Dijon, 1994' outro.jpg;
feh -x --fullscreen 'outro.jpg' & disown; sleep $LONG;

convert -background black -bordercolor black -gravity center -pointsize 90 -size 1280 -border 100 -fill DodgerBlue -font OSP-DIN.ttf caption:'Violet Hunt\nA Hard Woman: A Story in Scenes\nAppleton, New York, 1895' outro.jpg;
feh -x --fullscreen 'outro.jpg' & disown; sleep $LONG;

convert -background black -bordercolor black -gravity center -pointsize 90 -size 1280 -border 100 -fill white -font OSP-DIN.ttf caption:'Tina Modotti\nPhaidon Press, New York, 2002' outro.jpg;
feh -x --fullscreen 'outro.jpg' & disown; sleep $LONG;

convert -background black -bordercolor black -gravity center -pointsize 90 -size 1280 -border 100 -fill white -font OSP-DIN.ttf caption:'Blanche Selva\nCloches dans la Brume (for piano)\n1905' outro.jpg;
feh -x --fullscreen 'outro.jpg' & disown; sleep $LONG;

convert -background black -bordercolor black -gravity center -pointsize 90 -size 1280 -border 100 -fill white -font OSP-DIN.ttf caption:'scans + text corrections\n\nMichael Korntheuer\nNajet Boulafdal\nDonatella Portoghese' outro.jpg;
feh -x --fullscreen 'outro.jpg' & disown; sleep $DELAY;

convert -background black -bordercolor black -gravity center -pointsize 90 -size 1280 -border 100 -fill white -font OSP-DIN.ttf caption:'piano\n\nCaroline James' outro.jpg;
feh -x --fullscreen 'outro.jpg' & disown; sleep $DELAY;

convert -background black -bordercolor black -gravity center -pointsize 90 -size 1280 -border 100 -fill white -font OSP-DIN.ttf caption:'recording + mix\n\nGérald Wang' outro.jpg;
feh -x --fullscreen 'outro.jpg' & disown; sleep $DELAY;

convert -background black -bordercolor black -gravity center -pointsize 90 -size 1280 -border 100 -fill white -font OSP-DIN.ttf caption:'tools\n\npython, nltk, imagemagick, mplayer, feh, gitorious' outro.jpg;
feh -x --fullscreen 'outro.jpg' & disown; sleep $DELAY;

convert -background black -bordercolor black -gravity center -pointsize 90 -size 1280 -border 100 -fill white -font OSP-DIN.ttf caption:'typeface\n\nOSP-DIN' outro.jpg;
feh -x --fullscreen 'outro.jpg' & disown; sleep $DELAY;

convert -background black -bordercolor black -gravity center -pointsize 90 -size 1280 -border 100 -fill white -font OSP-DIN.ttf caption:'realisation\n\nFemke Snelting\nAn Mertens' outro.jpg;
feh -x --fullscreen 'outro.jpg' & disown; sleep $DELAY;

convert -background black -bordercolor black -gravity center -pointsize 90 -size 1280 -border 100 -fill white -font OSP-DIN.ttf caption:'Constant\nBruxxel, 2014' outro.jpg;
feh -x --fullscreen 'outro.jpg' & disown; sleep $DELAY;

convert -background black -bordercolor black -gravity center -pointsize 90 -size 1280 -border 100 -fill white -font OSP-DIN.ttf caption:'this is a free work, you can copy, distribute, and modify it under the terms of the Free Art License  http://artlibre.org' outro.jpg;
feh -x --fullscreen 'outro.jpg' & disown; sleep $DELAY;

convert -background black -bordercolor black -gravity center -pointsize 90 -size 1280 -border 100 -fill white -font OSP-DIN.ttf caption:'http://publicdomainday.constantvzw.org' outro.jpg;
feh -x --fullscreen 'outro.jpg' & disown; sleep $DELAY;

convert -background black -bordercolor black -gravity center -fill black -size 1280x800 -border 100 -font OSP-DIN.ttf caption:'Patience please!' outro.jpg;
feh -x --fullscreen 'intro.jpg' & disown; sleep $LONG;
