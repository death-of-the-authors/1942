#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from random import *
import nltk
from time import sleep
import sys
from datetime import datetime
import string

# Open all files and do basic cleanup

neel = open('neel.txt', 'r')
neel = neel.read()
neel = neel.replace("\r\n", " ")
neel = neel.replace("\r", " ")
neel = neel.replace("\n", " ")
neel = neel.replace("  ", " ")
neel = neel.replace("\"", " ")
	
violet = open('violet.txt', 'r')
violet = violet.read()
violet = violet.replace("\r\n", " ")
violet = violet.replace("\r", " ")
violet = violet.replace("\n", " ")
violet = violet.replace("  ", " ")
violet = violet.replace("\"", " ")

# Split text into sentences with help of nltk

sent_tokenizer=nltk.data.load('tokenizers/punkt/english.pickle')
sentences_neel = sent_tokenizer.tokenize(neel)

sent_tokenizer=nltk.data.load('tokenizers/punkt/english.pickle')
sentences_violet = sent_tokenizer.tokenize(violet)

# Limiting amount of words, to avoid illegible slides.

sentences_neel_select = []
sentences_violet_select = []

for sentence_neel in sentences_neel:
	words_neel = string.split(sentence_neel)
    	wordCount = len(words_neel)
	if wordCount < 20:
		sentences_neel_select.append(sentence_neel)

for sentence_violet in sentences_violet:
	words_violet = string.split(sentence_violet)
    	wordCount = len(words_violet)
	if wordCount < 20:
		sentences_violet_select.append(sentence_violet)
	
dialogues = (sentences_neel_select, sentences_violet_select)
total = 22
version = 1
time = 218 # length of blanche.mp3 = 218 sec. Limiting length of sentences made a difference too; less variation.

delay = time/total

# Intro starts here

cmd = "bash intro.sh"
os.system(cmd)

cmd ="convert -background black -bordercolor black -gravity center -fill black -size 1280x800 -border 100 -font OSP-DIN.ttf caption:'Patience please!' intro.jpg;"
os.system(cmd)
cmd="feh -x --fullscreen 'intro.jpg' &"
os.system(cmd)
sleep(delay/2)
	
# Generated versions start

while version <= 3:

	# Reset all counters
	
	starttime=datetime.now()
	element = "images"
	rest = 0
	extra = 0
	currenttotal = 0
	amount_selected_elements = 0
	
	# Let the music start
	
	cmd = "mplayer -really-quiet blanche/blanche.mp3 &"
	os.system(cmd)
	
	# taking out 'version' now (replaced by timestamp)
	
	#cmd ="convert -background black -bordercolor black -gravity center -pointsize 220 -size 1280 -border 100 -fill white -font OSP-DIN.ttf caption:'" + str(version) + "' intro.jpg;"
	#os.system(cmd)
	#cmd="feh -x --fullscreen 'intro.jpg' &"
	#os.system(cmd)
	#sleep(delay)
	
	# timestamp
	cmd ="convert -background black -bordercolor black -gravity center -pointsize 220 -size 1280 -border 100 -fill white -font OSP-DIN.ttf caption:'" + starttime.ctime() + "' intro.jpg;"
	os.system(cmd)
	cmd="feh -x --fullscreen 'intro.jpg' &"
	os.system(cmd)
	sleep(delay)
	
	cmd ="convert -background black -bordercolor black -gravity center -fill black -size 1280x800 -border 100 -font OSP-DIN.ttf caption:'Patience please!' intro.jpg;"
	os.system(cmd)
	cmd="feh -x --fullscreen 'intro.jpg' &"
	os.system(cmd)
	sleep(delay/2)
	
	# adding title slides manually to currenttotal
	
	currenttotal = 2

	# inner loop starts

	while currenttotal < total:
	
		# print "Started inner loop"
	
		if element == "images":
	
			amount_selected_elements = randint(1, 2)
		
			if currenttotal + amount_selected_elements > total:
				# print "stop!"
				break
			else:
				# print "currenttotal:", currenttotal, "amount_selected_elements:", amount_selected_elements, "elementtype:", element 
				z = 1
				while z <= amount_selected_elements:
		
					img = str(randint(1, 56)) + ".jpg"
					cmd = "feh -x -Z --fullscreen --image-bg black tina/" + img + " &"
					os.system(cmd)

					sleep(delay)
					z += 1
					# print "image: ", img
		
				currenttotal += amount_selected_elements
				element = "texts"

		else:
	
			amount_selected_elements = randint(1, 3)
		
			if currenttotal + amount_selected_elements > total:
				# print "stop!"
				break
		
			else:
				# print "currenttotal:", currenttotal, "amount_selected_elements:", amount_selected_elements, "elementtype:", element
				y = 1
				while y <= amount_selected_elements:
		
					dialogue = choice(dialogues)
					if dialogue == sentences_neel_select:
						colour = "firebrick1"
					if dialogue == sentences_violet_select:
						colour = "DodgerBlue"
				
					dialogue_pos = randrange(len(dialogue))
					slide = str(dialogue[dialogue_pos]) 
					text_file = open("slide.txt", "w")
					text_file.writelines(slide)
					text_file.close()
					img =  "'" + str(y) + "_text.jpg'"
					# print "text: ", img

					cmd = "convert -background black -bordercolor black -gravity center -fill " + colour + " -size 1024x768 -border 50 -font OSP-DIN.ttf caption:@slide.txt " + img +"; feh -x --fullscreen " + img + " &"
					os.system(cmd)
			
					sleep(delay)
					y += 1
		
				currenttotal += amount_selected_elements
				element = "images"
			
	# print "this is the end of the main loop"
	# print "currenttotal:", currenttotal, "elementtype:", element 
	# print "now patch up until total reached"

	while currenttotal < total:
		img = str(randint(1, 56)) + ".jpg"
			
		# print "image: ", img
		cmd = "feh -x -Z --fullscreen --image-bg black tina/" + img + " &"
		os.system(cmd)

		sleep(delay)
		currenttotal += 1
		z += 1
	
	# measuring length of each remix
	
	endtime=datetime.now()
		
	cmd ="convert -background black -bordercolor black -gravity center -fill black -size 1280x800 -border 100 -font OSP-DIN.ttf caption:'Patience please!' intro.jpg;"
	os.system(cmd)
	cmd="feh -x --fullscreen 'intro.jpg' &"
	os.system(cmd)
	sleep(delay)
	
	print "Version:", version, "Duration:", (endtime-starttime).seconds
	
	version += 1

# credits start here

cmd = "bash outro.sh"
os.system(cmd)	

# really the end

cmd = "killall -w feh;"
os.system(cmd)

# print 'FIN'

cmd = "clear"
os.system(cmd)

print "FIN"
