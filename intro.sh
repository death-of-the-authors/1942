#!/bin/bash

DELAY=6
SHORT=1
LONG=9
TIMESTAMP=$(date)

convert -background black -bordercolor black -gravity center -fill black -size 1280x800 -border 100 -font OSP-DIN.ttf caption:'Patience please!' intro.jpg;
feh -x --fullscreen 'intro.jpg' & disown; sleep $DELAY;

convert -background black -bordercolor black -gravity center -pointsize 220 -size 1280 -border 100 -fill white -font OSP-DIN.ttf caption:'THE DEATH\nOF THE\nAUTHORS' intro.jpg;
feh -x --fullscreen 'intro.jpg' & sleep $DELAY;

convert -background black -bordercolor black -gravity center -pointsize 90 -size 1280 -border 100 -fill white -font OSP-DIN.ttf caption:'a public domain remix' intro.jpg;
feh -x --fullscreen 'intro.jpg' & sleep $DELAY;

convert -background black -bordercolor black -gravity center -fill black -size 1280x800 -border 100 -font OSP-DIN.ttf caption:'Patience please!' intro.jpg;
feh -x --fullscreen 'intro.jpg' & disown; sleep $DELAY;

convert -background black -bordercolor black -gravity center -pointsize 220 -size 1280 -border 100 -fill white -font OSP-DIN.ttf caption:'1942' intro.jpg;
feh -x --fullscreen 'intro.jpg' & disown; sleep $DELAY;

convert -background black -bordercolor black -gravity center -pointsize 90 -size 1280 -border 100 -fill white -font OSP-DIN.ttf caption:'Neel Doff, Violet Hunt, Tina Modotti and Blanche Selva die on different continents, in very different circumstances' intro.jpg;
feh -x --fullscreen 'intro.jpg' & disown; sleep $LONG;

convert -background black -bordercolor black -gravity center -pointsize 220 -size 1280 -border 100 -fill white -font OSP-DIN.ttf caption:'70 years later' intro.jpg;
feh -x --fullscreen 'intro.jpg' & disown; sleep $DELAY;

convert -background black -bordercolor black -gravity center -pointsize 220 -size 1280 -border 100 -fill white -font OSP-DIN.ttf caption:'71 years later' intro.jpg;
feh -x --fullscreen 'intro.jpg' & disown; sleep $SHORT;

convert -background black -bordercolor black -gravity center -pointsize 220 -size 1280 -border 100 -fill white -font OSP-DIN.ttf caption:'72 years later' intro.jpg;
feh -x --fullscreen 'intro.jpg' & disown; sleep $SHORT;

convert -background black -bordercolor black -gravity center -fill black -size 1280x800 -border 100 -font OSP-DIN.ttf caption:'Patience please!' intro.jpg;
feh -x --fullscreen 'intro.jpg' & disown; sleep $SHORT;

convert -background black -bordercolor black -gravity center -pointsize 90 -size 1280 -border 100 -fill white -font OSP-DIN.ttf caption:'an unlikely encounter\nbetween four women' intro.jpg;
feh -x --fullscreen 'intro.jpg' & disown; sleep $DELAY;

convert -background black -bordercolor black -gravity center -fill black -size 1280x800 -border 100 -font OSP-DIN.ttf caption:'Patience please!' intro.jpg;
feh -x --fullscreen 'intro.jpg' & disown; sleep $SHORT;

convert -background black -bordercolor black -gravity center -pointsize 90 -size 1280 -border 100 -fill white -font OSP-DIN.ttf caption:'dialogue' intro.jpg;
feh -x --fullscreen 'intro.jpg' & disown; sleep $SHORT;

convert -background black -bordercolor black -gravity center -pointsize 220 -size 1280 -border 100 -fill white -font OSP-DIN.ttf caption:'NEEL DOFF\nVIOLET HUNT' intro.jpg;
feh -x --fullscreen 'intro.jpg' & disown; sleep $DELAY;

convert -background black -bordercolor black -gravity center -pointsize 90 -size 1280 -border 100 -fill white -font OSP-DIN.ttf caption:'photography' intro.jpg;
feh -x --fullscreen 'intro.jpg' & disown; sleep $SHORT;

convert -background black -bordercolor black -gravity center -pointsize 220 -size 1280 -border 100 -fill white -font OSP-DIN.ttf caption:'TINA MODOTTI' intro.jpg;
feh -x --fullscreen 'intro.jpg' & disown; sleep $DELAY;

convert -background black -bordercolor black -gravity center -pointsize 90 -size 1280 -border 100 -fill white -font OSP-DIN.ttf caption:'soundtrack' intro.jpg;
feh -x --fullscreen 'intro.jpg' & disown; sleep $SHORT;

convert -background black -bordercolor black -gravity center -pointsize 220 -size 1280 -border 100 -fill white -font OSP-DIN.ttf caption:'BLANCHE SELVA' intro.jpg;
feh -x --fullscreen 'intro.jpg' & disown; sleep $DELAY;
